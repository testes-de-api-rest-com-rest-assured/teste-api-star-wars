package apistarwars;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import org.hamcrest.Matchers;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class Planetas {

    @Test

    public void acessarSWAPIPlanets1() {

        given()
                .log().all()
        .when()
                .get("https://swapi.dev/api/planets/1/")
        .then()
                .log().all()
                .statusCode(200)
                .body("name", Matchers.containsString("Tatoo"))

        ;
    }

    @Test

    public void acessarSWAPIPlanetsNome1() {

        given()
                .log().all()
        .when()
                .get("https://swapi.dev/api/planets/")
        .then()
                .log().all()
                .statusCode(200)
                .body("results.name", Matchers.hasItem("Tatooine"))
        ;
    }

    @Test

    public void acessarSWAPIPlanetsNome3() {

        given()
                .log().all()
        .when()
                .get("https://swapi.dev/api/planets/3/")
        .then()
                .log().all()
                .statusCode(200)
                .body("name", Matchers.is("Yavin IV"))
        ;
    }


}
