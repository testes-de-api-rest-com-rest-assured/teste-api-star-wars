package apistarwars;

import org.junit.Test;

import static io.restassured.RestAssured.given;

public class TestesNegativos {

    @Test
    public void acessarPlanetaInexistente() {

        given()
          .log().all()
        .when()
          .get("https://swapi.dev/api/planets/100")
        .then()
          .log().all()
          .statusCode(404)
        ;
    }

    @Test
    public void acessarVeiculoInexistente(){

            given()
                    .log().all()
            .when()
                    .get("https://swapi.dev/apivehicles/52/")
            .then()
                    .log().all()
                    .statusCode(404)
            ;

    }

}
