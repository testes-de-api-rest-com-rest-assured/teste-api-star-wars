package apistarwars;

import org.hamcrest.Matchers;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class Filmes {

    @Test

    public void acessarSWAPIFilms() {

        given()
                .log().all()
        .when()
                .get("https://swapi.dev/api/films/")
        .then()
                .log().all()
                .statusCode(200)

                ;
    }

    @Test

    public void contagemSWAPIFilms() {

        given()
                .log().all()
                .when()
                .get("https://swapi.dev/api/films/")
                .then()
                .log().all()
                .statusCode(200)
                .body("count", Matchers.is(6))

        ;
    }


}
