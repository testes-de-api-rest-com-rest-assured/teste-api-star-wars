package apistarwars;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsInRelativeOrder;
import static org.hamcrest.Matchers.is;

import java.util.HashMap;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Test;

import io.restassured.http.ContentType;

public class AcessoSWAPI {

	@Test

	public void acessarSWAPI() {

		given()
			.log().all()
		.when()
			.get("https://swapi.dev/")
		.then()
			.log().all()
			.statusCode(200)
		;

	}
}