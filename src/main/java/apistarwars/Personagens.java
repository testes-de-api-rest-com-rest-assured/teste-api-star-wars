package apistarwars;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.hamcrest.Matchers;
import org.junit.Test;

public class Personagens {


	@Test

	public void acessarSWAPIPeople1() {

		given()
				.log().all()
		.when()
				.get("https://swapi.dev/api/people/1")
		.then()
				.log().all()
				.statusCode(200)
				.body("name", is("Luke Skywalker"))
				.body("eye_color", is("blue"))
		;

	}
}